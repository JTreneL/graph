from matplotlib import pyplot as plt



Panzergranate39 = [138,124,112,99,89]
Panzergranate40 = [194,174,149,127,106]
x = [100,500,1000,1500,2000]



def main():
    plt.title("Penetration data 7.5 cm KwK 42")
    plt.ylabel("Penetration milimetres:") 
    plt.xlabel("Range meters:")
    plt.plot(x,Panzergranate39,"Gold",label="Panzergranate39")
    plt.plot(x,Panzergranate40,"Black",label="Panzergranate40")
    plt.grid()
    plt.legend()
    plt.axis()
    plt.show()



if __name__ == "__main__":
    main()